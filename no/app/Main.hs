{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers #-}
module Main where

import Distribution.Simple.SetupHooks
import qualified Data.List.NonEmpty as NE
import Control.Monad.IO.Class


{-# NOINLINE bindingSetupHooks #-}
bindingSetupHooks (unit :: ()) = do
  let cmd = mkCommand (static Dict) (static (\ (_unit :: ()) -> print ())) unit

  _ <- liftIO $ return ()

  registerRule_ "rule1" $
    staticRule cmd [] (NE.fromList $ map undefined undefined)



main :: IO ()
main = putStrLn "Hello, Haskell!"
